#
# TP3 IA
# Algorithme MINMAX & ALPHA-BETA
# HODZIC Nihad
#
#############################################################################

import math
import numbers

class Noeud:
    Valeur = None
    Childs = []

    def __init__(self, *valeurs):
        if len(valeurs) == 1 and isinstance(valeurs[0], numbers.Number):
            self.Valeur = valeurs[0]
        else:
            self.Childs = valeurs

def minmax(noeud, depth, joueurMax):
    if depth == 0:
        return noeud.Valeur

    if joueurMax:
        evalmax = -math.inf
        for child in noeud.Childs:
            evalmax = max(evalmax, minmax(child, depth - 1, False))
        
        return evalmax

    else:
        evalmin = math.inf
        for child in noeud.Childs:
            evalmin = min(evalmin, minmax(child, depth - 1, True))
        
        return evalmin

def minmax_alpha_beta(noeud, depth, alpha, beta, joueurMax):
    if depth == 0:
        return noeud.Valeur

    if joueurMax:
        evlmax = -math.inf
        for child in noeud.Childs:
            evl = minmax_alpha_beta(child, depth - 1, alpha, beta, False)
            evlmax = max(evlmax, evl)
            alpha = max(alpha, evl)
            if beta <= alpha:
                break
        
        return evlmax

    else:
        evlmin = math.inf
        for child in noeud.Childs:
            evl = minmax_alpha_beta(child, depth - 1, alpha, beta, True)
            evlmin = min(evlmin, evl)
            beta = min(beta, evl)
            if beta <= alpha:
                break
        
        return evlmin

jeu = Noeud(
    Noeud(
        Noeud(
            Noeud(
                Noeud(4),
                Noeud(3),
                Noeud(8),
            ),
            Noeud(
                Noeud(2),
                Noeud(1)
            )
        ),
        Noeud(
            Noeud(
                Noeud(4),
                Noeud(2),
                Noeud(3),
            )
        ),
        Noeud(
            Noeud(
                Noeud(6),
                Noeud(4)
            ),
            Noeud(
                Noeud(7)
            ),
            Noeud(
                Noeud(5),
                Noeud(2)
            )
        )
    ),
    Noeud(
        Noeud(
            Noeud(
                Noeud(1),
                Noeud(9),
                Noeud(0)
            )
        ),
        Noeud(
            Noeud(
                Noeud(4),
                Noeud(3)
            ),
            Noeud(
                Noeud(0)
            )
        ),
        Noeud(
            Noeud(
                Noeud(2),
                Noeud(8),
                Noeud(4)
            ),
            Noeud(
                Noeud(3),
                Noeud(7)
            ),
            Noeud(
                Noeud(5),
                Noeud(4),
                Noeud(1)
            )
        )
    )
)

print("Résultat: " + str(minmax_alpha_beta(jeu, 4, -math.inf, math.inf, True)))