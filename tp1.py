#
# TP1 IA
# Algorithme A*
#
#############################################################################

import math
from outils import Ensemble, Graphe, Noeud, Arete

# Function g(n)
def g(graphe, n, noeud_depart):
    if n == noeud_depart:
        return 0
    else:
        return g(graphe, n.Precedent, noeud_depart) + graphe.getArete(n, n.Precedent, False).Distance

# Fonction heuristique h(n)
# n  : noeud courant
# nb : noeud but
def h(graphe, n, nb):
    if n == nb:
        return 0
    
    return math.sqrt((n.x - nb.x)**2 + (n.y - nb.y)**2 + (n.z - nb.z)**2)
    
# Function f(n)
def f(graphe, n, noeud_depart, noeud_but):
    return g(graphe, n, noeud_depart) + h(n, noeud_but)

def visualiser(ouvert, fermee):
    print("Ouvert: " + str(ouvert))
    print("Fermee: " + str(fermee))
    print("--------------------------------------------")

def algorithme_a_etoile(graphe, noeud_depart, noeud_but):
    # Initialisation, avant de commencer
    noeud_depart.Cout = 0
    noeud_depart.Precedent = None

    # Soit Ouvert un ensemble vide de noeuds
    Ouvert = Ensemble()

    # Soit Fermee un ensemble vide de noeuds
    Fermee = Ensemble()
    
    # Inserer le noeud de depart dans Ouvert
    Ouvert.ajouterDernier(noeud_depart)

    # Affichage de deux ensembles
    visualiser(Ouvert, Fermee)

    while not Ouvert.estVide():  # Faire tant que Ouvert n'est pas vide
        
        # Soit n le noeud enlevé de la tête de Ouvert
        n = Ouvert.extrairePremier()

        # Placer le noeud n dans Fermee
        Fermee.ajouterDernier(n)

        # Tous les successeurs de n désormais pointent vers n
        for successeur in n.Successeurs:
            if successeur == n.Precedent:
                continue

            if successeur.Precedent != None:
                successeur.PrecedentAux = successeur.Precedent

            successeur.Precedent = n

        # Si l'un des successeurs de n est le noeud but
        if noeud_but in n.Successeurs: 
            # Solution trouvée
            Solution = Ensemble()

            # Générer et retourner la solution
            noeud = noeud_but
            while noeud != noeud_depart:
                Solution.ajouterPremier(noeud)
                noeud = noeud.Precedent

            Solution.ajouterPremier(noeud_depart)
            # Affichage de deux ensembles
            visualiser(Ouvert, Fermee)
            return Solution

        # Pour tout successeurs N(n') de n
        for N in n.Successeurs:
            if N == n.Precedent:
                continue
        
            # Calculer g(n')
            gN = g(graphe, N, noeud_depart)

            # Calculer f(n')
            fN = gN + h(graphe, N, noeud_but)

            # Si N n'est ni dans Ouvert ni dans Ferme
            if not N in Ouvert and not N in Fermee:  
                # Ajouter N dans Ouvert
                Ouvert.ajouterDernier(N)

            # Sinon
            else:
                # Si l'ancienne valeur de g(n') est inférieure ou égale à la nouvelle
                if N.Cout <= gN:
                    # Continuer
                    N.Precedent = N.PrecedentAux
                    continue
                
                else:
                    # Si N figure dans Fermee
                    if N in Fermee:
                        # Le remettre dans Ouvert
                        Fermee.extraire(N)
                        Ouvert.ajouterDernier(N)
            
            # Garder les valeurs
            N.Cout = gN
            N.ValeurTri = fN
                    
            # Trier Ouvert par ordre croissant
            Ouvert.trier()

        # Affichage de deux ensembles
        visualiser(Ouvert, Fermee)
    
    # Aucune solution trouvée
    return None


# Exécution

graphe = Graphe()

A = Noeud("A")
B = Noeud("B")
C = Noeud("C")
D = Noeud("D")
E = Noeud("E")
F = Noeud("F")
G = Noeud("G")
H = Noeud("H")

A.x = 0
A.y = 0
A.z = 0

B.x = 0
B.y = 1
B.z = 0

C.x = 1
C.y = 1
C.z = 0

D.x = 2
D.y = 0
D.z = 0

E.x = 2
E.y = 2
E.z = 0

F.x = 1
F.y = 2
F.z = 0

G.x = 2
G.y = 2
G.z = 2

H.x = 0
H.y = 2
H.z = 2

graphe.ajouterNoeuds(A, B, C, D, E, F, G, H)
graphe.ajouterAretes(
    Arete(A, B, 1, False),
    Arete(A, D, 2, False),
    Arete(B, C, 1, False),
    Arete(B, F, 2, False),
    Arete(B, H, 3, False),
    Arete(C, F, 1, False),
    Arete(D, E, 2, False),
    Arete(D, G, 4, False),
    Arete(E, F, 1, False),
    Arete(E, G, 2, False),
    Arete(F, H, 3, False),
    Arete(G, H, 2, False))

print("--------------------------------------------")
Solution = algorithme_a_etoile(graphe, E, B)
if Solution == None:
    print("Aucune solution trouvée")
else:
    print("Solution: " + str(Solution) + "\n")