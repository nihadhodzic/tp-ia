#
# TP2 IA
# La Tour de Hanoi
#
#############################################################################

from outils import Ensemble, Graphe, Noeud, Arete

def visualiser(ouvert, fermee):
    print("Ouvert: " + str(ouvert))
    print("Fermee: " + str(fermee))
    print("--------------------------------------------")

# Exercice 1

def tour_de_hanoi(nb_disques, D, I, A):
    if nb_disques > 0:
        tour_de_hanoi(nb_disques - 1, D, A, I)
        print("Deplacer la disque de la quille " + D + " vers la quille " + A)
        tour_de_hanoi(nb_disques - 1, I, D, A)

"""
print("Veuillez saisir le nombre de disques: ")
nb_disques = int(input())
tour_de_hanoi(nb_disques, "Depart", "Intermediaire", "Arrivee")
"""



# Exercice 2

"""
Algorithme Best First Search
"""
def best_first_search(graphe, noeud_depart):
    noeud_depart.Cout = noeud_depart.ValeurHeuristique
    noeud_depart.Precedent = None

    # Soit Ouvert un ensemble vide de noeuds
    Ouvert = Ensemble()

    # Soit Fermee un ensemble vide de noeuds
    Fermee = Ensemble()  

    # Inserer le noeud de depart dans Ouvert
    Ouvert.ajouterDernier(noeud_depart)

    # Affichage de deux ensembles
    visualiser(Ouvert, Fermee)

    while not Ouvert.estVide():  # Faire tant que Ouvert n'est pas vide

        # Soit n le noeud enlevé de la tête de Ouvert
        n = Ouvert.extrairePremier()

        # Placer le noeud n dans Fermee
        Fermee.ajouterDernier(n)

        # Tous les successeurs de n désormais pointent vers n
        for successeur in n.Successeurs:
            if successeur.Precedent != None:
                successeur.PrecedentAux = successeur.Precedent

            successeur.Precedent = n

            # Si l'un des successeurs de n est le noeud but
            if successeur.ValeurHeuristique == 0:
                # Solution trouvée
                Solution = Ensemble()

                # Générer et retourner la solution
                noeud = successeur
                while noeud != noeud_depart:
                    Solution.ajouterPremier(noeud)
                    noeud = noeud.Precedent
                
                Solution.ajouterPremier(noeud_depart)

                # Affichage de deux ensembles
                visualiser(Ouvert, Fermee)

                return Solution

        
        
        # Pour tout successeurs N(n') de n
        for N in n.Successeurs:

            # Calculer h(n')
            eval_hN = n.Cout + N.ValeurHeuristique

            # Si N n'est ni dans Ouvert ni dans Ferme
            if not N in Ouvert and not N in Fermee:  
                # Ajouter N dans Ouvert
                Ouvert.ajouterDernier(N)

            else:
                # Si l'ancienne évaluation heuristique n' est inférieure ou égale à la nouvelle
                if N.Cout <= eval_hN:
                    # Continuer
                    N.Precedent = N.PrecedentAux
                    continue
                
                else:
                    # Si N figure dans Fermee
                    if N in Fermee:
                        # Le remettre dans Ouvert
                        Fermee.extraire(N)
                        Ouvert.ajouterDernier(N)
        
            # Appliquer les valeurs
            N.Cout = eval_hN
            N.ValeurTri = eval_hN
                    
            # Trier Ouvert par ordre croissant
            Ouvert.trier()

        # Affichage de deux ensembles
        visualiser(Ouvert, Fermee)

    # Aucune solution trouvée
    return None

"""
Algorithme Branch and Bound
"""
def branch_and_bound(graphe, noeud_depart):
    # Initialisation
    noeud_depart.Precedent = None
    noeud_depart.Cout = noeud_depart.ValeurHeuristique

    # Soit Ouvert un ensemble vide de noeuds
    Ouvert = Ensemble()

    # Soit Fermee un ensemble vide de noeuds
    Fermee = Ensemble()  

    # Inserer le noeud de depart dans Ouvert
    Ouvert.ajouterDernier(noeud_depart)

    # Affichage de deux ensembles
    visualiser(Ouvert, Fermee)

    while not Ouvert.estVide():  # Faire tant que Ouvert n'est pas vide

        # Soit n le noeud enlevé de la tête de Ouvert
        n = Ouvert.extrairePremier()

        # Placer le noeud n dans Fermee
        Fermee.ajouterDernier(n)

        # Tous les successeurs de n désormais pointent vers n
        for successeur in n.Successeurs:
            if successeur.Precedent != None:
                successeur.PrecedentAux = successeur.Precedent

            successeur.Precedent = n

            # Si l'un des successeurs de n est le noeud but
            if successeur.ValeurHeuristique == 0:
                # Solution trouvée
                Solution = Ensemble()

                # Générer et retourner la solution
                noeud = successeur
                while noeud != noeud_depart:
                    Solution.ajouterPremier(noeud)
                    noeud = noeud.Precedent
                
                Solution.ajouterPremier(noeud)

                # Affichage de deux ensembles
                visualiser(Ouvert, Fermee)

                return Solution

        # Pour tout successeurs N(n') de n
        for N in n.Successeurs:
            # Calculer f(n')
            fN = n.Cout + graphe.getArete(n, N).Distance

            # Si N n'est ni dans Ouvert ni dans Ferme
            if not N in Ouvert and not N in Fermee:
                # Ajouter N dans Ouvert
                Ouvert.ajouterDernier(N)

            # Sinon
            else:
                # Si l'ancienne valeur de f(n') est inférieure ou égale à la nouvelle
                if N.Cout <= fN:
                    # Continuer
                    N.Precedent = N.PrecedentAux
                    continue
                
                else:
                    # Si N figure dans Fermee
                    if N in Fermee:
                        # Le remettre dans Ouvert
                        Fermee.extraire(N)
                        Ouvert.ajouterDernier(N)
            
            # Appliquer les valeurs
            N.Cout = fN
            N.ValeurTri = fN
                    
            # Trier Ouvert par ordre croissant
            Ouvert.trier()
        
        # Affichage de deux ensembles
        visualiser(Ouvert, Fermee)
    
    # Aucune solution trouvée
    return None

"""
Algorithme A*
"""
def algorithme_a_etoile(graphe, noeud_depart):
    # Initialisation, avant de commencer
    noeud_depart.Cout = 0
    noeud_depart.Precedent = None

    # Soit Ouvert un ensemble vide de noeuds
    Ouvert = Ensemble()

    # Soit Fermee un ensemble vide de noeuds
    Fermee = Ensemble()
    
    # Inserer le noeud de depart dans Ouvert
    Ouvert.ajouterDernier(noeud_depart)

    # Affichage de deux ensembles dans la console
    visualiser(Ouvert, Fermee)

    while not Ouvert.estVide():  # Faire tant que Ouvert n'est pas vide
        
        # Soit n le noeud enlevé de la tête de Ouvert
        n = Ouvert.extrairePremier()

        # Placer le noeud n dans Fermee
        Fermee.ajouterDernier(n)

        # Tous les successeurs de n désormais pointent vers n
        for successeur in n.Successeurs:
            if successeur.Precedent != None:
                successeur.PrecedentAux = successeur.Precedent

            successeur.Precedent = n

            # Si l'un des successeurs de n est le noeud but
            if successeur.ValeurHeuristique == 0:
                # Solution trouvée
                Solution = Ensemble()

                # Générer et retourner la solution
                noeud = successeur
                while noeud != noeud_depart:
                    Solution.ajouterPremier(noeud)
                    noeud = noeud.Precedent

                Solution.ajouterPremier(noeud_depart)

                # Affichage de deux ensembles
                visualiser(Ouvert, Fermee)
    
                return Solution

        # Pour tout successeurs N(n') de n
        for N in n.Successeurs:
            # Calculer g(n')
            gN = n.Cout + graphe.getArete(n, N).Distance

            # Calculer f(n')
            fN = gN + N.ValeurHeuristique

            # Si N n'est ni dans Ouvert ni dans Ferme
            if not N in Ouvert and not N in Fermee:  
                # Ajouter N dans Ouvert
                Ouvert.ajouterDernier(N)

            # Sinon
            else:
                # Si l'ancienne valeur de g(n') est inférieure ou égale à la nouvelle
                if N.Cout <= gN:
                    # Continuer
                    N.Precedent = N.PrecedentAux
                    continue
                
                else:
                    # Si N figure dans Fermee
                    if N in Fermee:
                        # Le remettre dans Ouvert
                        Fermee.extraire(N)
                        Ouvert.ajouterDernier(N)
            
            # Appliquer les valeurs
            N.Cout = gN
            N.ValeurTri = fN
                    
            # Trier Ouvert par ordre croissant
            Ouvert.trier()

        # Affichage de deux ensembles
        visualiser(Ouvert, Fermee)
    
    # Aucune solution trouvée
    return None



# Execution Exercice 2
graphe = Graphe()

A = Noeud("A")
B = Noeud("B")
C = Noeud("C")
D = Noeud("D")
E = Noeud("E")
S = Noeud("S")
G1 = Noeud("G1")
G2 = Noeud("G2")

A.ValeurHeuristique = 2
B.ValeurHeuristique = 1
C.ValeurHeuristique = 3
D.ValeurHeuristique = 1
E.ValeurHeuristique = 6
S.ValeurHeuristique = 5
G1.ValeurHeuristique = 0
G2.ValeurHeuristique = 0

graphe.ajouterNoeuds(
    A, B, C, D, E, S, G1, G2
)

graphe.ajouterAretes(
    Arete(A, B, 1),
    Arete(A, E, 8),
    Arete(B, C, 1),
    Arete(B, D, 1),
    Arete(B, S, 2),
    Arete(B, G1, 4),
    Arete(C, D, 1),
    Arete(C, G2, 5),
    Arete(D, G1, 5),
    Arete(D, G2, 1),
    Arete(E, G1, 9),
    Arete(E, G2, 7),
    Arete(S, A, 2),
    Arete(S, C, 3),
    Arete(G1, S, 4))

print("--------------------------------------------")
Solution = best_first_search(graphe, S)
#Solution = branch_and_bound(graphe, S)
#Solution = algorithme_a_etoile(graphe, S)

if Solution == None:
    print("Aucune solution trouvée")
else:
    print("Solution: " + str(Solution) + "\n")