# Classe Graphe
class Graphe:

  def __init__(self):
    self.Noeuds = []
    self.Aretes = []

  def taille(self):
    return len(self.Noeuds)

  def ajouterNoeuds(self, *noeuds):
    for noeud in noeuds:
      self.Noeuds.append(noeud)

  def ajouterAretes(self, *aretes):
    for arete in aretes:
      self.Aretes.append(arete)

  def getArete(self, n1, n2, ordre=False):
    for arete in self.Aretes:
      if (arete.Depart == n1 and arete.Arrivee == n2) or not ordre and (arete.Depart == n2 and arete.Arrivee == n1):
        return arete
    
    return None

# Class Noeud
class Noeud:
  def __init__(self, nom):
    self.Nom = nom
    self.ValueurTri = None
    self.Cout = None
    self.ValeurHeuristique = None
    self.Precedent = None
    self.PrecedentAux = None
    self.Successeurs = []
    self.Predecesseurs = []

  def ajouterSuccesseur(self, noeud):
    self.Successeurs.append(noeud)

  def ajouterPredecesseur(self, noeud):
    self.Predecesseurs.append(noeud)
  
  def __str__(self):
    return self.Nom


# Classe Arete
class Arete:
    def __init__(self, depart, arrivee, distance, oriente=True):
        self.Depart = depart
        self.Arrivee = arrivee
        if not oriente:
          arrivee.ajouterSuccesseur(depart)
      
        depart.ajouterSuccesseur(arrivee)
        self.Distance = distance


# Classe Ensemble
class Ensemble:
  def __init__(self):
    self.Elements = []

  def ajouterPremier(self, element):
    self.Elements.insert(0, element)

  def ajouterDernier(self, element):
    self.Elements.append(element)

  def extrairePremier(self):
    return self.Elements.pop(0)

  def extraireDernier(self):
    return self.Elements.pop(len(self.Elements) - 1)

  def extraire(self, element):
    self.Elements.remove(element)

  def trier(self, ordreDecroissant=False):
    self.Elements.sort(key=lambda x: x.ValeurTri, reverse=ordreDecroissant)

  def estVide(self):
    return not self.Elements

  def __contains__(self, element):
    return (element in self.Elements)

  def __str__(self):
    length = len(self.Elements)
    res = "["

    for i in range(length):
      res += self.Elements[i].Nom
      if(i + 1 != length):
        res += ", "

    return res + "]"